#!../../bin/linux-x86_64/motorSim
< envPaths

dbLoadDatabase("$(TOP)/dbd/motorSim.dbd")
motorSim_registerRecordDeviceDriver(pdbbase)
epicsEnvSet("SYS", "$(IOC_SYS)")
epicsEnvSet("DEV", "$(IOC_DEV)")


set_requestfile_path(".", "")

set_savefile_path("/opt/epics/autosave/$(IOC_SYS)_$(IOC_DEV)")
set_pass0_restoreFile("settings.sav","SYS=$(SYS), DEV=$(DEV)")
save_restoreSet_DatedBackupFiles(0)


# Use a local configuration if axes need individual customization
dbLoadTemplate("motorSim.substitutions", "P=$(IOC_SYS):$(IOC_DEV):")

< motorSim.cmd


iocInit

create_monitor_set("settings.req",5, "SYS=$(SYS), DEV=$(DEV)")

## motorUtil (allstop & alldone)
motorUtilInit("$(IOC_SYS):$(IOC_DEV):")
# Boot complete
date

